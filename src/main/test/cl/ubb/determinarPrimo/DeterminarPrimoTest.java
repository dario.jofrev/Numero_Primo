package cl.ubb.determinarPrimo;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
public class DeterminarPrimoTest {

	@Test
	public void ingresoCeroRetornaFalla() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo ();
		boolean resultado;
		
		/*act*/
		resultado = primo.determinarPrimo(0);
		
		/*assert*/
		assertThat(resultado,is(false));
	}

}
